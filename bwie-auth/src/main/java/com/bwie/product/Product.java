package com.bwie.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bwie.product.dao")
public class Product {

    public static void main(String[] args) {
        SpringApplication.run(Product.class);
    }
}
