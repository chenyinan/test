package com.bwie.product.controller;


import com.bwie.common.domain.request.Car;
import com.bwie.common.domain.request.Order;
import com.bwie.common.domain.request.RulesValues;
import com.bwie.common.result.Result;
import com.bwie.product.service.ProService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
public class proController {


    @Autowired
    private ProService proService;


    @PostMapping(value = "/list")
    public Result<List<Order>> list(){

        Result<List<Order>> result=proService.list();

        return result;
    }


    @PostMapping(value = "/carList")
    public Result<List<Car>> carList(){

        Result<List<Car>>result=proService.carList();

        return result;
    }


    @PostMapping(value = "/insert")
    public Result insert(@RequestBody Order order){

     Result result=   proService.insert(order);



     return result;
    }


}
