package com.bwie.product.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.bwie.common.domain.request.Car;
import com.bwie.common.domain.request.Order;
import com.bwie.common.result.Result;
import com.bwie.product.dao.ProDao;
import com.bwie.product.service.ProService;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProServiceImpl implements ProService {

    @Autowired
    private ProDao proDao;

    @Override
    public Result<List<Order>> list() {
        ArrayList<Order> orders = new ArrayList<>();

       List<Order> list=proDao.list();

       list.forEach(l->{
           lists(l);

       });



        return Result.success(list);
    }

    private void lists(Order l) {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

        KieSession kieSession = kieClasspathContainer.newKieSession();
        kieSession.insert(l);

        kieSession.fireAllRules();

        kieSession.dispose();
    }

    @Override
    public Result<List<Car>> carList() {
        ArrayList<Car> cars = new ArrayList<>();

        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieClasspathContainer.newKieSession();

        List<Car> cars1=proDao.carList();

        cars1.forEach(l->{
            carDule(l);
        });

        return null;
    }

    @Override
    public Result insert(Order order) {


       List<Order> orders= proDao.find();

     /*   orders.forEach(l->{
            Class<? extends Order> aClass = l.getClass();
            try {
                Field l1 = aClass.getDeclaredField("l");
                String name = l1.getName();

                try {
                    Method declaredMethod = aClass.getDeclaredMethod(name, Order.class);

                    declaredMethod.invoke()
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }

            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }


            *//*      l.getClass().getDeclaredMethod("setNumber",Order.class).*//*
           System.out.println(l);
          Listers(l);
       });*/

        return Result.success(orders);
    }

    private void Listers(Order l) {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieClasspathContainer.newKieSession();
        kieSession.insert(l);
        kieSession.fireAllRules();

        kieSession.dispose();
    }

    private void carDule(Car l) {

        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

        KieSession kieSession = kieClasspathContainer.newKieSession();


        kieSession.insert(l);

        kieSession.fireAllRules();

        kieSession.dispose();
    }
}
