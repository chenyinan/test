package com.bwie.product.service;

import com.bwie.common.domain.request.Car;
import com.bwie.common.domain.request.Order;
import com.bwie.common.result.Result;

import java.util.List;

public interface ProService {
    Result<List<Order>> list();

    Result<List<Car>> carList();

    Result insert(Order order);

}
