package com.bwie.product.dao;

import com.bwie.common.domain.request.Car;
import com.bwie.common.domain.request.Order;

import java.util.List;

public interface ProDao {
    List<Order> list();

    List<Car> carList();

    void insert(Order order);

    List<Order> find();

}
