import com.bwie.common.domain.request.Order;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.mindrot.jbcrypt.BCrypt;

public class User {

@Test
public void test(){
    KieServices kieServices = KieServices.Factory.get();

    KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();

    KieSession kieSession = kieClasspathContainer.newKieSession();


    Order order = new Order();
    order.setNumber("320411200208116718");
    order.setPhone("18626");


    kieSession.insert(order);

    kieSession.fireAllRules();

    kieSession.dispose();

    System.out.println(order.getCode());
    System.out.println(order.getNumber());

    boolean checkpw = BCrypt.checkpw("18626", order.getCode());


    if (checkpw){
        System.out.println("解密成功");
    }else {
        System.out.println("解密失败");
    }
}
}
