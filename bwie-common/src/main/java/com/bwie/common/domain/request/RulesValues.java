package com.bwie.common.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class RulesValues {

    private Integer id;
    private String name;
    private String value;
    private Integer typeId;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:ss:mm")
    @JsonFormat(pattern = "yyyy-MM-dd hh:ss:mm",timezone = "GTM+8")
    private Date createDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:ss:mm")
    @JsonFormat(pattern = "yyyy-MM-dd hh:ss:mm",timezone = "GTM+8")
    private Date updaDate;

}
