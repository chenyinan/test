package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Car {
    private Integer carId;
    private String carNumber;
    private String phone;
    private String card;
}
