package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class RuleType {
    private Integer id;
    private String typeName;
}
