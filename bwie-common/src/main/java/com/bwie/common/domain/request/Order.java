package com.bwie.common.domain.request;

import lombok.Data;

@Data
public class Order {
    private String number;
    private String code;
    private String phone;
}
